import 'package:app_monitor/api/models/transaction.dart';
import 'package:app_monitor/api/providers/transaction.provider.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:lipsum/lipsum.dart' as lipsum;

class DashboardView extends StatefulWidget {
  DashboardView({Key key}) : super(key: key);

  @override
  _DashboardViewState createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {
  TooltipBehavior _tooltipBehavior;
  bool isDashboardLoading = true;
  bool testMode = true;

  @override
  void initState() {
    _tooltipBehavior = TooltipBehavior(enable: true);
    super.initState();

    //
  }

  @override
  Widget build(BuildContext context) {
    TransactionProvider transProvider =
        Provider.of<TransactionProvider>(context, listen: false);

    transProvider.getTransactionList().then((value) {
      if (this.isDashboardLoading) {
        setState(() {
          this.isDashboardLoading = !this.isDashboardLoading;
        });
      }
    });

    return Expanded(
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 0,
          vertical: 0,
        ),
        decoration: BoxDecoration(
            // color: Colors.amber.shade200,
            ),
        child: Column(
          children: [
            //graphique
            Expanded(
              flex: 4,
              child: Container(
                decoration: BoxDecoration(
                  color: Color(0xffFAFAFA),
                ),
                child: this.testMode
                    ? SfCartesianChart(
                        primaryXAxis: CategoryAxis(),
                        // Titre graphique
                        title: ChartTitle(text: 'Analyse journalière'),
                        // activer legend
                        legend: Legend(
                          isVisible: true,
                          isResponsive: true,
                          position: LegendPosition.bottom,
                          alignment: ChartAlignment.center,
                        ),
                        // activer tooltip
                        tooltipBehavior: _tooltipBehavior,
                        series: <LineSeries<Trans, String>>[
                          // Operateur P02
                          LineSeries<Trans, String>(
                            name: "ORANGE",
                            dataSource: [
                              Trans(230000, "06"),
                              Trans(300000, "07"),
                              Trans(310000, "08"),
                              Trans(500000, "09"),
                              Trans(210000, "10"),
                              Trans(160000, "11"),
                              Trans(110000, "12"),
                              Trans(520000, "13"),
                              Trans(700000, "14"),
                              Trans(410000, "15"),
                            ].map((transaction) => transaction).toList(),
                            xValueMapper: (Trans trans, _) => trans.heure,
                            yValueMapper: (Trans trans, _) => trans.montant,

                            // activer data label
                            dataLabelSettings: DataLabelSettings(
                                isVisible: false,
                                alignment: ChartAlignment.center,
                                labelAlignment: ChartDataLabelAlignment.auto,
                                textStyle: TextStyle(fontSize: 5)),
                          ),
                          // Operateur P03
                          LineSeries<Trans, String>(
                            name: "MOOV",
                            dataSource: [
                              Trans(110000, "06"),
                              Trans(120000, "07"),
                              Trans(180000, "08"),
                              Trans(210000, "09"),
                              Trans(100000, "10"),
                              Trans(90000, "11"),
                              Trans(50000, "12"),
                              Trans(250000, "13"),
                              Trans(360000, "14"),
                              Trans(205000, "15"),
                            ].map((trans) => trans).toList(),
                            xValueMapper: (Trans trans, _) => trans.heure,
                            yValueMapper: (Trans trans, _) => trans.montant,

                            // activer data label
                            dataLabelSettings: DataLabelSettings(
                                isVisible: false,
                                alignment: ChartAlignment.center,
                                labelAlignment: ChartDataLabelAlignment.auto,
                                textStyle: TextStyle(fontSize: 5)),
                          ),
                          // Operateur P10
                          LineSeries<Trans, String>(
                            name: "ECOBANK",
                            dataSource: [
                              Trans(5000, "06"),
                              Trans(10000, "07"),
                              Trans(5000, "08"),
                              Trans(10000, "09"),
                              Trans(5000, "10"),
                              Trans(2500, "11"),
                              Trans(0, "12"),
                              Trans(20000, "13"),
                              Trans(50000, "14"),
                              Trans(20000, "15"),
                            ].map((trans) => trans).toList(),
                            xValueMapper: (Trans trans, _) => trans.heure,
                            yValueMapper: (Trans trans, _) => trans.montant,

                            // activer data label
                            dataLabelSettings: DataLabelSettings(
                                isVisible: false,
                                alignment: ChartAlignment.center,
                                labelAlignment: ChartDataLabelAlignment.auto,
                                textStyle: TextStyle(fontSize: 5)),
                          ),
                        ],
                      )
                    : (this.isDashboardLoading
                        ? Center(
                            child: Container(
                              decoration: BoxDecoration(
                                  // color: Colors.green,
                                  ),
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.white,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                  Colors.deepOrange,
                                ),
                              ),
                            ),
                          )
                        : SfCartesianChart(
                            primaryXAxis: CategoryAxis(),
                            // Titre graphique
                            title: ChartTitle(text: 'Analyse journalière'),
                            // activer legend
                            legend: Legend(
                              isVisible: true,
                              isResponsive: true,
                              position: LegendPosition.bottom,
                              alignment: ChartAlignment.center,
                            ),
                            // activer tooltip
                            tooltipBehavior: _tooltipBehavior,
                            series: <LineSeries<Transaction, String>>[
                              // Operateur P02
                              LineSeries<Transaction, String>(
                                name: "P02",
                                dataSource: transProvider
                                    .operatorsFilter("P02")
                                    .map((transaction) => transaction)
                                    .toList(),
                                xValueMapper: (Transaction trans, _) =>
                                    trans.hour,
                                yValueMapper: (Transaction trans, _) =>
                                    int.parse(trans.totalTrans),

                                // activer data label
                                dataLabelSettings: DataLabelSettings(
                                    isVisible: false,
                                    alignment: ChartAlignment.center,
                                    labelAlignment:
                                        ChartDataLabelAlignment.auto,
                                    textStyle: TextStyle(fontSize: 5)),
                              ),
                              // Operateur P03
                              LineSeries<Transaction, String>(
                                name: "P03",
                                dataSource: transProvider
                                    .operatorsFilter("p03")
                                    .map((transaction) => transaction)
                                    .toList(),
                                xValueMapper: (Transaction trans, _) =>
                                    trans.hour,
                                yValueMapper: (Transaction trans, _) =>
                                    int.parse(trans.totalTrans),

                                // activer data label
                                dataLabelSettings: DataLabelSettings(
                                    isVisible: false,
                                    alignment: ChartAlignment.center,
                                    labelAlignment:
                                        ChartDataLabelAlignment.auto,
                                    textStyle: TextStyle(fontSize: 5)),
                              ),
                              // Operateur P10
                              LineSeries<Transaction, String>(
                                name: "P10",
                                dataSource: transProvider
                                    .operatorsFilter("P10")
                                    .map((transaction) => transaction)
                                    .toList(),
                                xValueMapper: (Transaction trans, _) =>
                                    trans.hour,
                                yValueMapper: (Transaction trans, _) =>
                                    int.parse(trans.totalTrans),

                                // activer data label
                                dataLabelSettings: DataLabelSettings(
                                    isVisible: false,
                                    alignment: ChartAlignment.center,
                                    labelAlignment:
                                        ChartDataLabelAlignment.auto,
                                    textStyle: TextStyle(fontSize: 5)),
                              ),
                            ],
                          )),
                //
              ),
            ),
            //Résumé
            testMode
                ? SizedBox()
                : Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.symmetric(
                        horizontal: 10,
                        vertical: 0,
                      ),
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        color: Colors.red.shade200,
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                      ),
                      child: SingleChildScrollView(
                        child: AutoSizeText(
                          "RESUME: " +
                              lipsum.createParagraph(
                                numSentences: 3,
                              ),
                          overflow: TextOverflow.ellipsis,
                          softWrap: true,
                          maxLines: 5,
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}

class Trans {
  Trans(this.montant, this.heure);
  final String heure;
  final double montant;
}
