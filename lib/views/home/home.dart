import 'package:app_monitor/commons_widgets/my_dialog_box.dart';
import 'package:app_monitor/commons_widgets/my_header.dart';
import 'package:app_monitor/views/alerte/alerte_view.dart';
import 'package:app_monitor/views/dashboard/dashboard_view.dart';
import 'package:app_monitor/views/profil/profil_view.dart';
import 'package:app_monitor/views/rapport/rapport_view_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';

class HomeView extends StatefulWidget {
  HomeView({Key key}) : super(key: key);

  @override
  _HomeViewState createState() {
    return _HomeViewState();
  }
}

class _HomeViewState extends State<HomeView> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  Size screenSize;
  int indexView = 0;
  List<String> viewTitleList = [
    "Alerte",
    "Rapport",
    "Dashboard",
    "Profil",
  ];
  List<String> iconDataList = [
    "assets/icons/bell_icon.svg",
    "assets/icons/rapport_icon.svg",
    "assets/icons/dashboard_icon.svg",
    "assets/icons/avatar_icon.svg",
  ];

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () async {
        var platform = Theme.of(context).platform;
        if (platform == TargetPlatform.android) {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return CustomDialogBox(
                  title: "Fermeture !",
                  descriptions: "Voulez-vous fermer l'application ?",
                );
              },);
          return true;
        }

        return false;
      },
      child: Scaffold(
        key: _scaffoldKey,
        // Bar de navigation
        bottomNavigationBar: this.myBottomNavigationBar(),
        //Container de toute la vue
        body: Container(
          height: screenSize.height,
          width: screenSize.width,
          decoration: BoxDecoration(
            color: Colors.white70,
          ),
          child: Column(
            children: [
              MyHeader(
                screenSize,
                viewTitleList[indexView],
                iconDataList[indexView],
                setTitleView: indexView != 3 ? true : false,
                setArrowBack: indexView != 0 ? true : false,
              ),
              //Content
              indexView == 0
                  ? AlerteView(
                      scaffoldKey: _scaffoldKey,
                    )
                  : (indexView == 1
                      ? RapportView2(
                          scaffoldKey: _scaffoldKey,
                        )
                      : (indexView == 2 ? DashboardView() : ProfilView())),
            ],
          ),
        ),
      ),
    );
  }

  Widget myBottomNavigationBar() {
    return Container(
      height: screenSize.height * 0.10,
      padding: EdgeInsets.symmetric(),
      decoration: BoxDecoration(
        color: Color(0xFFFAFAFA),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(35),
          topRight: Radius.circular(35),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                  onTap: () {
                    setState(() {
                      indexView = 0;
                      this.hideBottomSheet();
                    });
                  },
                  child: SvgPicture.asset(
                    "assets/icons/home_icon.svg",
                    color: indexView == 0 ? Color(0xffE96700) : null,
                    height: 30,
                    width: 30,
                  )),
              Text(
                'Home',
                style: TextStyle(
                  color: indexView == 0 ? Color(0xffE96700) : Color(0xffC5C4BB),
                  fontSize: 12,
                  fontFamily: "Montserrat",
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                  onTap: () {
                    setState(() {
                      indexView = 1;
                      this.hideBottomSheet();
                    });
                  },
                  child: SvgPicture.asset(
                    iconDataList[1],
                    color: indexView == 1 ? Color(0xffE96700) : null,
                    height: 30,
                    width: 30,
                  )),
              Text(
                'Rapports',
                style: TextStyle(
                  color: indexView == 1 ? Color(0xffE96700) : Color(0xffC5C4BB),
                  fontSize: 12,
                  fontFamily: "Montserrat",
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                  onTap: () {
                    setState(() {
                      indexView = 2;
                      this.hideBottomSheet();
                    });
                  },
                  child: SvgPicture.asset(
                    iconDataList[2],
                    color: indexView == 2 ? Color(0xffE96700) : null,
                    height: 30,
                    width: 30,
                  )),
              Text(
                'Dashboard',
                style: TextStyle(
                  color: indexView == 2 ? Color(0xffE96700) : Color(0xffC5C4BB),
                  fontSize: 12,
                  fontFamily: "Montserrat",
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                  onTap: () {
                    setState(() {
                      indexView = 3;
                      this.hideBottomSheet();
                    });
                  },
                  child: SvgPicture.asset(
                    iconDataList[3],
                    color: indexView == 3 ? Color(0xffE96700) : null,
                    height: 30,
                    width: 30,
                  )),
              Text(
                'Profil',
                style: TextStyle(
                  color: indexView == 3 ? Color(0xffE96700) : Color(0xffC5C4BB),
                  fontSize: 12,
                  fontFamily: "Montserrat",
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void hideBottomSheet() {
    _scaffoldKey.currentState.showBottomSheet((context) => SizedBox());
  }
  //fin
}
