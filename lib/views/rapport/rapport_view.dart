import 'package:app_monitor/views/alerte/components/filter_component.dart';
import 'package:app_monitor/views/alerte/components/search_bar_component.dart';
import 'package:auto_size_text/auto_size_text.dart'; 
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class RapportView extends StatefulWidget {
  RapportView({Key key}) : super(key: key);

  @override
  _RapportViewState createState() => _RapportViewState();
}

class _RapportViewState extends State<RapportView> {
  List<Employee> employees = <Employee>[];

  EmployeeDataSource employeeDataSource;

  List<Employee> getEmployees() {
    return [
      Employee(
        10001,
        'James',
        'Project Lead',
        20000,
        "louloulexus@gmail.com",
        "0153488836",
      ),
      Employee(
        10002,
        'Kathryn',
        'Manager',
        30000,
        "louloulexus@gmail.com",
        "0153488836",
      ),
      Employee(
        10003,
        'Lara',
        'Developer',
        15000,
        "louloulexus@gmail.com",
        "0153488836",
      ),
      Employee(
        10004,
        'Michael',
        'Designer',
        15000,
        "louloulexus@gmail.com",
        "0153488836",
      ),
      Employee(
        10005,
        'Martin',
        'Developer',
        15000,
        "louloulexus@gmail.com",
        "0153488836",
      ),
      Employee(
        10006,
        'Newberry',
        'Developer',
        15000,
        "louloulexus@gmail.com",
        "0153488836",
      ),
      Employee(
        10007,
        'Balnc',
        'Developer',
        15000,
        "louloulexus@gmail.com",
        "0153488836",
      ),
      Employee(
        10008,
        'Perry',
        'Developer',
        15000,
        "louloulexus@gmail.com",
        "0153488836",
      ),
      Employee(
        10009,
        'Gable',
        'Developer',
        15000,
        "louloulexus@gmail.com",
        "0153488836",
      ),
      Employee(
        10010,
        'Grimes',
        'Developer',
        15000,
        "louloulexus@gmail.com",
        "0153488836",
      )
    ];
  }

  @override
  void initState() {
    super.initState();
    employees = getEmployees();
    employeeDataSource = EmployeeDataSource(employees: employees);
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Expanded(
      //Content
      child: Container(
        height: screenSize.height,
        width: screenSize.width,
        padding: EdgeInsets.symmetric(
          horizontal: 10,
        ),
        decoration: BoxDecoration(
            // color: Colors.amber.shade200,
            ),
        child: Column(
          children: [
            //search bar
            SearchBarComponent(screenSize: screenSize),
            SizedBox(
              height: 10,
            ),
            // Filter
            FilterComponent(),
            SizedBox(
              height: 10,
            ),
            //data table
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.grey.shade200,
                ),
                child: SfDataGrid(
                  source: employeeDataSource,
                  columns: <GridColumn>[
                    GridTextColumn(
                      columnName: 'mont_j_j',
                      label: Container(
                          padding: EdgeInsets.all(16.0),
                          alignment: Alignment.centerLeft,
                          child: SingleChildScrollView(
                            child: AutoSizeText(
                              'Montant Jour J',
                              maxLines: 2,
                            ),
                          )),
                    ),
                    GridTextColumn(
                      columnName: 'trans_j_j',
                      label: Container(
                          padding: EdgeInsets.all(16.0),
                          alignment: Alignment.centerLeft,
                          child: SingleChildScrollView(
                            child: AutoSizeText(
                              'Transactions Jour J',
                              maxLines: 2,
                            ),
                          )),
                    ),
                    GridTextColumn(
                      columnName: 'mont_j_m_1',
                      width: 120,
                      label: Container(
                          padding: EdgeInsets.all(16.0),
                          alignment: Alignment.centerLeft,
                          child: SingleChildScrollView(
                            child: AutoSizeText(
                              'Montant Jour M-1',
                              maxLines: 2,
                            ),
                          )),
                    ),
                    GridTextColumn(
                      columnName: 'trans_j_m_1',
                      label: Container(
                          padding: EdgeInsets.all(16.0),
                          alignment: Alignment.centerRight,
                          child: SingleChildScrollView(
                            child: AutoSizeText(
                              'Transactions Jour M-1',
                              maxLines: 2,
                            ),
                          )),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Employee {
  Employee(
      this.id, this.name, this.designation, this.salary, this.email, this.tel);
  final int id;
  final String name;
  final String email;
  final String tel;
  final String designation;
  final int salary;
}

class EmployeeDataSource extends DataGridSource {
  List<DataGridRow> _employees = [];

  EmployeeDataSource({List<Employee> employees}) {
    this._employees = employees
        .map<DataGridRow>(
          (e) => DataGridRow(
            cells: [
              DataGridCell<int>(columnName: 'mont_j_j', value: e.id),
              DataGridCell<String>(columnName: 'trans_j_j', value: e.name),
              DataGridCell<String>(
                  columnName: 'mont_j_m_1', value: e.designation),
              DataGridCell<int>(columnName: 'trans_j_m_1', value: e.salary),
            ],
          ),
        )
        .toList();
  }

  @override
  List<DataGridRow> get rows => this._employees;

  @override
  DataGridRowAdapter buildRow(DataGridRow row) {
    return DataGridRowAdapter(
      cells: row.getCells().map<Widget>(
        (dataGridCell) {
          return Container(
            alignment: (dataGridCell.columnName == 'id' ||
                    dataGridCell.columnName == 'salary')
                ? Alignment.centerLeft
                : Alignment.centerLeft,
            padding: EdgeInsets.all(16.0),
            child: Text(dataGridCell.value.toString()),
          );
        },
      ).toList(),
    );
  }
}
