import 'dart:async';

import 'package:app_monitor/api/providers/auth.provider.dart';
import 'package:app_monitor/commons_widgets/footer_component.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart'; 
import 'package:provider/provider.dart';

class MySplashScreen extends StatefulWidget {
  MySplashScreen({Key key}) : super(key: key);

  @override
  _MySplashScreenState createState() => _MySplashScreenState();
}

class _MySplashScreenState extends State<MySplashScreen>
    with TickerProviderStateMixin {
  Animation _containerRadiusAnimation,
      _containerSizeAnimation,
      _containerColorAnimation;
  AnimationController _containerAnimationController;

  @override
  void initState() {
    super.initState();
    _containerAnimationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 3000));

    _containerRadiusAnimation = BorderRadiusTween(
            begin: BorderRadius.circular(200.0),
            end: BorderRadius.circular(0.0))
        .animate(CurvedAnimation(
            curve: Curves.ease, parent: _containerAnimationController));

    _containerSizeAnimation = Tween(begin: 0, end: 0.486).animate(
        CurvedAnimation(
            curve: Curves.ease, parent: _containerAnimationController));

    _containerColorAnimation =
        ColorTween(begin: Colors.deepOrangeAccent, end: Colors.white).animate(
            CurvedAnimation(
                curve: Curves.ease, parent: _containerAnimationController));

    _containerAnimationController.forward();

    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);

    authProvider.getUserStatus().then((String navigation) {
      if (navigation == "intro") {
        Timer(Duration(milliseconds: 3600), () {
          Navigator.pushReplacementNamed(context, "/intro");
        });
      }
      if (navigation == "login") {
        Timer(Duration(milliseconds: 3600), () {
          Navigator.pushReplacementNamed(context, "/login");
        });
      }
      if (navigation == "home") {
        Timer(Duration(milliseconds: 3600), () {
          Navigator.pushReplacementNamed(context, "/home");
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;

    return Scaffold(
      bottomSheet: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FooterComponent(),
          Container(
            width: width,
            decoration: BoxDecoration(
              color: Color(0xFFEFEFEF),
            ),
            padding: EdgeInsets.only(
              bottom: 20,
              top: 10,
              left: 15,
              right: 15,
            ),
            child: Text(
              "V 1.0.0",
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
      body: Container(
        width: width,
        height: height,
        color: Colors.white,
        child: AnimatedBuilder(
          animation: _containerAnimationController,
          builder: (context, index) {
            return Container(
              transform: Matrix4.translationValues(
                  _containerSizeAnimation.value * width - 200, 0, 0.0),
              width: _containerSizeAnimation.value * width,
              height: _containerSizeAnimation.value * height,
              decoration: BoxDecoration(
                borderRadius: _containerRadiusAnimation.value,
                color: _containerColorAnimation.value,
              ),
              child: Center(
                  child: SvgPicture.asset(
                'assets/images/logo_full.svg',
                fit: BoxFit.contain,
              )
                  // Text(
                  //   'AppMonitor',
                  //   style: TextStyle(
                  //     color: Colors.black,
                  //     fontSize: 23,
                  //   ),
                  // ),
                  ),
            );
          },
        ),
      ),
    );
  }
}
