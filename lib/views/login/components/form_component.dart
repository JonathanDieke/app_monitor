import 'package:app_monitor/api/providers/auth.provider.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class FormComponent extends StatefulWidget {
  FormComponent({Key key}) : super(key: key);

  @override
  _FormComponentState createState() => _FormComponentState();
}

class _FormComponentState extends State<FormComponent> {
  TextEditingController loginTextController = TextEditingController(text: '');
  TextEditingController pwdTextController = TextEditingController(text: '');

  bool isRememberMe = false,
      isAttemptLogin = false,
      isObscurePswd = true,
      isErrorLoginTextField = false,
      isErrorPwdTextField = false;
  String errorMessage = 'Remplir ce champ';

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Container(
      decoration: BoxDecoration(
        // color: Colors.deepOrange,
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //Login Text
              Container(
                alignment: Alignment.topLeft,
                margin: EdgeInsets.only(top: 5),
                child: Text(
                  'Login'.toUpperCase(),
                  style: TextStyle(
                    color: Colors.black.withOpacity(0.7),
                    fontSize: 30,
                    fontWeight: FontWeight.w600,
                    fontFamily: "Montserrat",
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              //Afficher les erreurs
              this.isErrorLoginTextField
                  ? Text(
                      this.errorMessage,
                      textAlign: TextAlign.left,
                      style: TextStyle(color: Colors.red.shade700),
                    )
                  : SizedBox(
                      height: 0,
                    ),
              // Email
              Container(
                height: screenSize.height * 0.066,
                padding: EdgeInsets.symmetric(horizontal: 5),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.elliptical(13, 13)),
                  border: Border.all(
                    color: this.isErrorLoginTextField
                        ? Colors.red.shade700
                        : Colors.grey.shade300,
                    style: BorderStyle.solid,
                  ),
                ),
                child: TextField(
                  controller: loginTextController,
                  autocorrect: true,
                  onChanged: (String value) {
                    setState(() {
                      this.isErrorLoginTextField = false;
                    });
                  },
                  decoration: InputDecoration(
                      hintText: "Email",
                      hintStyle: TextStyle(
                        fontSize: 16,
                        fontFamily: "Arial",
                      ),
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                ),
              ),
              SizedBox(
                height: 25,
              ),
              //Password
              //Afficher les erreurs
              this.isErrorPwdTextField
                  ? Text(
                      this.errorMessage,
                      textAlign: TextAlign.left,
                      style: TextStyle(color: Colors.red.shade700),
                    )
                  : SizedBox(
                      height: 0,
                    ),
              Container(
                height: screenSize.height * 0.066,
                padding: EdgeInsets.symmetric(horizontal: 5),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: this.isErrorPwdTextField
                        ? Colors.red.shade700
                        : Colors.grey.shade300,
                    style: BorderStyle.solid,
                  ),
                ),
                child: TextField(
                  onChanged: (String value) {
                    setState(() {
                      this.isErrorPwdTextField = false;
                    });
                  },
                  controller: pwdTextController,
                  autocorrect: true,
                  obscureText: this.isObscurePswd,
                  decoration: InputDecoration(
                      suffix: GestureDetector(
                        child: (isObscurePswd)
                            ? Icon(
                                FontAwesomeIcons.solidEye,
                                color: Colors.black,
                                size: 20,
                              )
                            : Icon(
                                FontAwesomeIcons.eyeSlash,
                                color: Colors.black,
                                size: 20,
                              ),
                        onTap: () {
                          setState(() {
                            print("password");
                            if (isObscurePswd == true) {
                              isObscurePswd = false;
                            } else {
                              isObscurePswd = true;
                            }
                          });
                        },
                      ),
                      hintText: "Password",
                      hintStyle: TextStyle(
                        fontSize: 16,
                        fontFamily: "Arial",
                      ),
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              //Bouton de connexion
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        //Se souvenir de moi
                        Checkbox(
                          value: this.isRememberMe,
                          activeColor: Colors.grey.shade300,
                          checkColor: Colors.green,
                          // focusColor: Colors.amber,
                          // hoverColor: Colors.red,
                          onChanged: (bool value) {
                            setState(() {
                              this.isRememberMe = value;
                            });
                          },
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              this.isRememberMe = !this.isRememberMe;
                            });
                          },
                          child: Text(
                            "Se souvenir de moi",
                            style: TextStyle(
                              fontFamily: "Montserrat",
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 25),
                    //Bouton de connexion
                    Container(
                      constraints: BoxConstraints(
                        minWidth: screenSize.width,
                        minHeight: 50.0,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(
                          Radius.elliptical(10, 10),
                        ),
                      ),
                      child: !isAttemptLogin
                          // ignore: deprecated_member_use
                          ? RaisedButton(
                              onPressed: () {
                                this.attemptLogin();
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.elliptical(10, 10),
                                ),
                              ),
                              padding: EdgeInsets.all(0.0),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.all(
                                    Radius.elliptical(10, 10),
                                  ),
                                ),
                                child: Container(
                                  constraints: BoxConstraints(
                                    minWidth: screenSize.width,
                                    minHeight: 50.0,
                                  ),
                                  alignment: Alignment.center,
                                  child: Text(
                                    "Connexion",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 17,
                                      fontFamily: "Montserrat",
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : Center(
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.green,
                                ),
                                child: CircularProgressIndicator(
                                  backgroundColor: Colors.green,
                                  strokeWidth: 5,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                    Colors.white,
                                  ),
                                ),
                              ),
                            ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  attemptLogin() async {
    // Navigator.pushReplacementNamed(context, "/home");

    if (this.loginTextController.text.isEmpty ||
        this.loginTextController.text == null ||
        this.pwdTextController.text.isEmpty ||
        this.pwdTextController.text == null) {
      //
      if (this.loginTextController.text.isEmpty ||
          this.loginTextController.text == null) {
        setState(() {
          this.isErrorLoginTextField = true;
        });
      }

      if (this.pwdTextController.text.isEmpty ||
          this.pwdTextController.text == null) {
        setState(() {
          this.isErrorPwdTextField = true;
        });
      }
      // this._showSnackbar('Veuillez remplir tous les champs !',
      //     removePwd: false);
    } else {
      setState(() {
        isAttemptLogin = true;
      });

      AuthProvider authProvider =
          Provider.of<AuthProvider>(context, listen: false);

      // Navigator.pushReplacementNamed(context, "/home");
      //Make with love, by JonathanDieke
      authProvider
          .authenticate(loginTextController.text, pwdTextController.text, this.isRememberMe)
          .then((bool result) {
        print("result : $result");

        setState(() {
          isAttemptLogin = false;
        });

        if (result) {
          Navigator.pushReplacementNamed(context, "/home");
        } else {
          this._showSnackbar('Identifiants invalides, réessayez svp !');
        }
      }).onError((error, stackTrace) {
        print("error :" + error.toString());
        print("staackTrace: " + stackTrace.toString());
        setState(() {
          isAttemptLogin = false;
        });

        this._showSnackbar(
            "Quelque chose s'est mal passé : veuilez vérifier votre connexion internet !");
      });
    }

    //
  }

  _showSnackbar(String message, {bool removePwd = true}) {
    final snackBar = SnackBar(
      backgroundColor: Colors.grey[400],
      duration: Duration(milliseconds: 3000),
      elevation: 4,
      content: Text(
        message,
        style: TextStyle(color: Colors.black),
      ),
      action: SnackBarAction(
        label: 'OK',
        textColor: Colors.black,
        onPressed: () {
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
        },
      ),
    );

    if (removePwd) {
      pwdTextController.text = "";
    }

    // Find the ScaffoldMessenger in the widget tree
    // and use it to show a SnackBar.
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  //
}
