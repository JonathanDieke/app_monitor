import 'package:app_monitor/commons_widgets/footer_component.dart';
import 'package:app_monitor/commons_widgets/logo.dart';
import 'package:app_monitor/views/login/components/form_component.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginView extends StatefulWidget {
  LoginView({Key key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      bottomSheet: FooterComponent(),
      body: Container(
        height: screenSize.height,
        width: screenSize.width,
        child: ListView(
          children: [
            //Header
            Container(
              height: screenSize.height * 0.2,
              decoration: BoxDecoration(),
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    logoMin(),
                    GestureDetector(
                      onTap: () async {
                        SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        if (prefs.getBool("userAttemptedConnection")) {
                          //Sortir de l'application
                          print('close app');
                          SystemChannels.platform
                              .invokeMethod('SystemNavigator.pop');
                        } else {
                          Navigator.pushReplacementNamed(context, "/intro");
                        }
                      },
                      child: Icon(Icons.close, size: 30, color: Colors.black),
                    ), 
                  ],
                ),
              ),
            ),
            //Formulaire
            FormComponent(),
          ],
        ),
      ),
    );
  }
}
