import 'package:app_monitor/commons_widgets/footer_component.dart';
import 'package:app_monitor/commons_widgets/logo.dart';
import 'package:app_monitor/views/intro/components/page_view_1.dart';
import 'package:app_monitor/views/intro/components/page_view_2.dart';
import 'package:app_monitor/views/intro/components/page_view_3.dart';
import 'package:app_monitor/views/login/login_view.dart';
import 'package:expandable_page_view/expandable_page_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:page_transition/page_transition.dart';
import 'package:page_view_indicators/page_view_indicators.dart';

class IntroView extends StatefulWidget {
  IntroView({Key key}) : super(key: key);

  @override
  _IntroViewState createState() => _IntroViewState();
}

class _IntroViewState extends State<IntroView> {
  PageController _pageController = PageController(
    initialPage: 0,
  );

  ValueNotifier<int> _indicatorValue = ValueNotifier<int>(0);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      bottomSheet: FooterComponent(),
      body: Column(
        children: [
          //header
          Container(
            height: screenSize.height * 0.20,
            decoration: BoxDecoration(color: Colors.white),
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 20,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  logoMin(),
                  GestureDetector(
                    onTap: () {
                      //Sortir de l'application
                      SystemChannels.platform
                          .invokeMethod('SystemNavigator.pop');
                    },
                    child: Icon(Icons.close, size: 30, color: Colors.black),
                  ),
                ],
              ),
            ),
          ),
          //Content
          Container(
            height: screenSize.height * 0.8,
            width: screenSize.width,
            padding: EdgeInsets.only(bottom: 40),
            decoration: BoxDecoration(color: Colors.white),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                //Page Views
                Container(
                  child: ExpandablePageView(
                    onPageChanged: (int pageNumber) {
                      setState(() {
                        _indicatorValue.value = pageNumber;
                      });
                    },
                    controller: _pageController,
                    children: [
                      PageView1(),
                      PageView2(),
                      PageView3(),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                this._buildCircleIndicator2(),
                SizedBox(height: 15),
                //Bouton connexion
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        type: PageTransitionType.bottomToTop,
                        child: LoginView(),
                      ),
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.all(
                        Radius.elliptical(10, 10),
                      ),
                    ),
                    child: Container(
                      constraints: BoxConstraints(
                        minWidth: screenSize.width,
                        minHeight: 50.0,
                      ),
                      alignment: Alignment.center,
                      child: Text(
                        "Connexion",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontFamily: "Montserrat",
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCircleIndicator2() {
    return CirclePageIndicator(
      size: 16.0,
      selectedSize: 18.0,
      itemCount: 3,
      currentPageNotifier: _indicatorValue,
    );
  }

//
}
