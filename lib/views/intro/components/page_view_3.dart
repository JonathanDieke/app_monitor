import 'package:flutter/material.dart'; 

class PageView3 extends StatelessWidget {
  const PageView3({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Column(
      children: [
        Container(
          height: screenSize.height * 0.45,
          width: screenSize.width,
          margin: EdgeInsets.symmetric(
            horizontal: 20,
          ),
          child: Image.asset("assets/images/slider_3.jpeg"),
        ),
        Container(
          child: Text(
            'Connectez-vous et soyez au contrôle !',
            textDirection: TextDirection.ltr,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 25,
              fontFamily: "Seravek",
            ),
          ),
        )
      ],
    );
  }
}
