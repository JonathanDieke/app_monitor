import 'package:flutter/material.dart';

class PageView2 extends StatelessWidget {
  const PageView2({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Column(
      children: [
        Container(
          height: screenSize.height * 0.45,
          width: screenSize.width,
          margin: EdgeInsets.symmetric(
            horizontal: 20,
          ),
          child: Image.asset("assets/images/godseye_logo.png"),
          // SvgPicture.asset("assets/images/logo_full_2.svg"),
        ),
        Container(
          child: Text(
            'Plus rien ne vous échappera..',
            textDirection: TextDirection.ltr,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 25,
              fontFamily: "Seravek",
            ),
          ),
        )
      ],
    );
  }
}
