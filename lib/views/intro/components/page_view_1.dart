import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PageView1 extends StatelessWidget {
  const PageView1({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Column(
      children: [
        Container(
          height: screenSize.height * 0.45,
          width: screenSize.width,
          margin: EdgeInsets.symmetric(
            horizontal: 20,
          ),
          child: SvgPicture.asset("assets/images/slider_1.svg"),
        ),
        Container(
          padding: EdgeInsets.symmetric(
            vertical: 3,
            horizontal: 10,
          ),
          child: Text(
            "Suivez en temps réel l'évolution des graphiques...",
            textAlign: TextAlign.center,
            textDirection: TextDirection.ltr,
            style: TextStyle(
              fontSize: 25,
              fontFamily: "Seravek",
            ),
          ),
        )
      ],
    );
  }

//
}
