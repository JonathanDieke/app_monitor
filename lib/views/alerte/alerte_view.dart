import 'package:app_monitor/api/providers/alerte.provider.dart';
import 'package:app_monitor/api/providers/auth.provider.dart';
import 'package:app_monitor/utils/constants.dart';
import 'package:app_monitor/utils/local_notifications_manager.dart'; 
import 'package:app_monitor/views/alerte/components/alerte_item.dart'; 
import 'package:app_monitor/views/alerte/components/search_bar_component.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class AlerteView extends StatefulWidget {
  AlerteView({Key key, this.scaffoldKey}) : super(key: key);

  final GlobalKey<ScaffoldState> scaffoldKey;

  @override
  _AlerteViewState createState() => _AlerteViewState();
}

class _AlerteViewState extends State<AlerteView> {
  Size screenSize;
  AlerteProvider alerteProvider;
  AuthProvider authProvider;
  bool isAlerteLoading = true;
  PersistentBottomSheetController _bottomSheetController;
  String company = "CIE",
      operateur = "ORANGE",
      date = "05-07-2021",
      heure = "13h";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    alerteProvider = Provider.of<AlerteProvider>(context, listen: false);
    alerteProvider.getAlertList().then((value) {
      if (isAlerteLoading) {
        setState(() {
          isAlerteLoading = false;
        });
      }
    });

    alerteProvider.getAlertList().then((value) {
      if (isAlerteLoading) {
        setState(() {
          isAlerteLoading = !isAlerteLoading;
        });
      }
    });

    screenSize = MediaQuery.of(context).size;
 
    // var localNotify = LocalNotifyManager.init();
    // localNotify.runNotifications();

    return Expanded(
      child: Container(
        margin: EdgeInsets.only(
          top: 10,
          right: 10,
          left: 10,
        ),
        decoration: BoxDecoration(
            // color: Colors.amber.shade200,
            ),
        child: Column(
          children: [
            //Bar de recherhce et filtre
            Column(
              children: [
                //Bar de recherche
                SearchBarComponent(screenSize: screenSize),
                SizedBox(
                  height: 10,
                ),
                //Réglages des options
                // FilterComponent(scaffoldKey: widget.scaffoldKey),
                filterComponent(),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
            //Liste des alertes
            Expanded(
              child: this.isAlerteLoading
                  ? Center(
                      //Loader
                      child: Container(
                        decoration: BoxDecoration(
                            // color: Colors.green,
                            ),
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.white,
                          valueColor: AlwaysStoppedAnimation<Color>(
                            Colors.deepOrange,
                          ),
                        ),
                      ),
                    )
                  : SingleChildScrollView(
                      child: Container(
                        width: screenSize.width,
                        decoration: BoxDecoration(
                            // color: Colors.blue.shade400,
                            ),
                        child: (alerteProvider.alerteList.isNotEmpty
                            ? Column(
                                //Liste alertes
                                children: alerteProvider.alerteList
                                    .map((alerte) => AlerteItem(
                                        screenSize: screenSize, alerte: alerte))
                                    .toList(),
                              )
                            : Container(
                                //Aucune alerte trouvée
                                padding: EdgeInsets.symmetric(
                                  horizontal: 40,
                                  vertical: 40,
                                ),
                                child: Center(
                                  child: Text('Aucune donnée pour le moment !'),
                                ),
                              )),
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }

  Widget filterComponent() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          height: screenSize.height * 0.066,
          child: Row(
            children: [
              //Icone de réglagle
              GestureDetector(
                onTap: () {
                  _bottomSheetController.closed.then((value) {
                    print(value);
                    if (value == null) {
                      this._showBottomSheet();
                    } else {
                      print('sheet bottom closed');
                      Navigator.pop(context);
                      _bottomSheetController.close();
                    }
                  });
                },
                child: SvgPicture.asset(
                  "assets/icons/filter_icon.svg",
                  fit: BoxFit.contain,
                  color: Colors.black,
                ),
              ),
              //Filtrer texte
              RichText(
                text: TextSpan(
                  text: "  Filtrer : ",
                  style: TextStyle(
                      color: Colors.black, fontSize: 18, fontFamily: "Arial"),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: Container(
            height: 35,
            decoration: BoxDecoration(),
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                optionItem(
                  "Tous",
                  color: Colors.deepOrange,
                  onTap: () {
                    print('tap on optionItem');
                  },
                ),
                optionItem(
                  "Compagnie",
                  color: Colors.grey,
                  items: ["CIE", "SODECI"],
                ),
                optionItem(
                  "Prestataires",
                  color: Colors.grey,
                  items: ["ORANGE", "MOOV", "ECOBANK"],
                ),
                optionItem(
                  "Jour",
                  color: Colors.grey,
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return this.showDateCustomDialog();
                        });
                  },
                ),
                optionItem(
                  "Heure",
                  color: Colors.grey,
                  onTap: () {
                    print('tap on optionItem');
                  },
                ),
              ],
            ),
          ),
        ),
        //
      ],
    );
  }

  Widget optionItem(String label,
      {Color color: Colors.grey, List<String> items, Function onTap}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 7),
      margin: EdgeInsets.only(right: 10),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(35),
      ),
      child: Row(
        children: [
          items != null
              ? DropdownButton<String>(
                  hint: Text(
                    ' ${label.toUpperCase()} ',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontFamily: "IBMPlexSans",
                    ),
                  ),
                  // value: this.company,
                  elevation: 5,
                  style: TextStyle(color: Colors.black),
                  items: items.map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (String value) {
                    this.company = value;
                    _bottomSheetController.close();
                  },
                )
              :
              //Quand pas de liste
              GestureDetector(
                  onTap: onTap,
                  child: Text(
                    ' ${label.toUpperCase()} ',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontFamily: "IBMPlexSans",
                    ),
                  ),
                ),
          // activatedIcon ? Icon(Icons.keyboard_arrow_down) : SizedBox(),
        ],
      ),
    );
  }

  void _showBottomSheet() {
    this._bottomSheetController =
        widget.scaffoldKey.currentState.showBottomSheet((context) {
      return myFilterBottomSheet();
    });
  }

  Widget myFilterBottomSheet() {
    return Container(
      padding: EdgeInsets.only(
        left: 30,
        right: 30,
      ),
      height: screenSize.height * 0.35, //260.0
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.elliptical(15, 15),
          topRight: Radius.elliptical(15, 15),
        ),
        color: Color(0xFFFAFAFA),
      ),
      child: ListView(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: Text(
              "Filtrer par item",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: "Seravek",
                fontSize: 18,
              ),
            ),
          ),
          Divider(
            height: 3,
            color: Color(0xFF000000),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Compagnie',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18, fontFamily: "IBMPlexSans"),
                ),
                DropdownButton<String>(
                  value: this.company,
                  elevation: 5,
                  style: TextStyle(color: Colors.black),
                  items: <String>[
                    'CIE',
                    'SODECI',
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (String value) {
                    this.company = value;
                    _bottomSheetController.close();
                  },
                ),
              ],
            ),
          ),
          Divider(
            height: 3,
            color: Color(0xFF000000),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Prestataire',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18, fontFamily: "IBMPlexSans"),
                ),
                DropdownButton<String>(
                  value: this.operateur,
                  elevation: 5,
                  style: TextStyle(color: Colors.black),
                  items: <String>[
                    'ORANGE',
                    'MOOV',
                    'SGBCI',
                    'ECOBANK',
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (String value) {
                    this.operateur = value;
                    _bottomSheetController.close();
                  },
                ),
              ],
            ),
          ),
          Divider(
            height: 3,
            color: Color(0xFF000000),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Date',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18, fontFamily: "IBMPlexSans"),
                ),
                GestureDetector(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return this.showDateCustomDialog();
                        });
                  },
                  child: Text(
                    this.date,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18, fontFamily: "IBMPlexSans"),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            height: 3,
            color: Color(0xFF000000),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Heure',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18, fontFamily: "IBMPlexSans"),
                ),
                Text(
                  '13h',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18, fontFamily: "IBMPlexSans"),
                ),
              ],
            ),
          ),
          Divider(
            height: 3,
            color: Color(0xFF000000),
          ),
          //Garder un espace avec la fin de la listview
          SizedBox(height: 5),
        ],
      ),
    );
  }

  Widget showDateCustomDialog() {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Constants.MY_DIALOG_BOX_PADDING),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              left: Constants.MY_DIALOG_BOX_PADDING,
              top: Constants.MY_DIALOG_BOX_PADDING,
              right: Constants.MY_DIALOG_BOX_PADDING,
            ),
            margin: EdgeInsets.only(top: Constants.MY_DIALOG_BOX_AVATAR_RADIUS),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.white,
                borderRadius: BorderRadius.circular(Constants.MY_DIALOG_BOX_PADDING),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black,
                      offset: Offset(0, 10),
                      blurRadius: 10),
                ]),
            child: SfDateRangePicker(
              view: DateRangePickerView.month,
              monthViewSettings:
                  DateRangePickerMonthViewSettings(firstDayOfWeek: 1),
              toggleDaySelection: true,
              showActionButtons: true,
              onSubmit: (calendarDate) {
                setState(() {
                  this.date = calendarDate.toString().split(" ")[0];
                  print(this.date);
                });
                Navigator.pop(context);
                _bottomSheetController.close();
              },
              onCancel: () {
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }

  //Fin
}
