import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FilterComponent extends StatefulWidget {
  FilterComponent({Key key, this.scaffoldKey}) : super(key: key);

  final GlobalKey<ScaffoldState> scaffoldKey;

  @override
  _FilterComponentState createState() => _FilterComponentState();
}

class _FilterComponentState extends State<FilterComponent> {
  String company = "CIE",
      operateur = "ORANGE",
      date = "05-07-2021",
      heure = "13h";
  Size screenSize;

  @override
  void initState() {
    super.initState();
  }

  void _showBottomSheet() {
    widget.scaffoldKey.currentState.showBottomSheet((context) {
      return myFilterBottomSheet();
    });
  }

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          height: screenSize.height * 0.066,
          child: Row(
            children: [
              //Icone de réglagle
              GestureDetector(
                onTap: () {
                  this._showBottomSheet();
                },
                child: SvgPicture.asset(
                  "assets/icons/filter_icon.svg",
                  fit: BoxFit.contain,
                  color: Colors.black,
                ),
              ),
              //Filtrer texte
              RichText(
                text: TextSpan(
                  text: "  Filtrer : ",
                  style: TextStyle(
                      color: Colors.black, fontSize: 18, fontFamily: "Arial"),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: Container(
            height: 35,
            decoration: BoxDecoration(),
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                optionItem("Tous",
                    activatedIcon: false, color: Colors.deepOrange, onTap: () {
                  print('tap on optionItem');
                }),
                optionItem("Compagnie", color: Colors.grey, onTap: () {
                  print('tap on optionItem');
                }),
                optionItem("Prestataires", color: Colors.grey, onTap: () {
                  print('tap on optionItem');
                }),
                optionItem("Jour", color: Colors.grey, onTap: () {
                  print('tap on optionItem');
                }),
                optionItem("Heure", color: Colors.grey, onTap: () {
                  print('tap on optionItem');
                }),
              ],
            ),
          ),
        ),
        //
      ],
    );
  }

  Widget optionItem(String label,
      {Color color: Colors.grey, activatedIcon: true, Function onTap}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 7),
        margin: EdgeInsets.only(right: 10),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(35),
        ),
        child: Row(
          children: [
            Text(
              ' ${label.toUpperCase()} ',
              style: TextStyle(
                fontSize: 16,
                color: Colors.white,
                fontFamily: "IBMPlexSans",
              ),
            ),
            activatedIcon ? Icon(Icons.keyboard_arrow_down) : SizedBox(),
          ],
        ),
      ),
    );
  }

  Widget myFilterBottomSheet() {
    return Container(
      padding: EdgeInsets.only(
        left: 30,
        right: 30,
      ),
      height: screenSize.height * 0.35, //260.0
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.elliptical(15, 15),
          topRight: Radius.elliptical(15, 15),
        ),
        color: Color(0xFFFAFAFA),
      ),
      child: ListView(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: Text(
              "Filtrer par item",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: "Seravek",
                fontSize: 18,
              ),
            ),
          ),
          Divider(
            height: 3,
            color: Color(0xFF000000),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Compagnie',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18, fontFamily: "IBMPlexSans"),
                ),
                DropdownButton<String>(
                  value: this.company,
                  elevation: 5,
                  style: TextStyle(color: Colors.black),
                  items: <String>[
                    'CIE',
                    'SODECI',
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (String value) {
                    this.company = value;
                    this._showBottomSheet();
                  },
                ),
              ],
            ),
          ),
          Divider(
            height: 3,
            color: Color(0xFF000000),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Prestataire',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18, fontFamily: "IBMPlexSans"),
                ),
                DropdownButton<String>(
                  value: this.operateur,
                  elevation: 5,
                  style: TextStyle(color: Colors.black),
                  items: <String>[
                    'ORANGE',
                    'MOOV',
                    'SGBCI',
                    'ECOBANK',
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (String value) {
                    this.operateur = value;
                    this._showBottomSheet();
                  },
                ),
              ],
            ),
          ),
          Divider(
            height: 3,
            color: Color(0xFF000000),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Date',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18, fontFamily: "IBMPlexSans"),
                ),
                Text(
                  '05-07-2021',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18, fontFamily: "IBMPlexSans"),
                ),
              ],
            ),
          ),
          Divider(
            height: 3,
            color: Color(0xFF000000),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Heure',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18, fontFamily: "IBMPlexSans"),
                ),
                Text(
                  '13h',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18, fontFamily: "IBMPlexSans"),
                ),
              ],
            ),
          ),
          Divider(
            height: 3,
            color: Color(0xFF000000),
          ),
          //Garder un espace avec la fin de la listview
          SizedBox(height: 5),
        ],
      ),
    );
  }
//
}
