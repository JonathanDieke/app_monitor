import 'package:app_monitor/api/providers/auth.provider.dart';
import 'package:app_monitor/utils/helpers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class ProfilView extends StatefulWidget {
  ProfilView({Key key}) : super(key: key);

  @override
  _ProfilViewState createState() => _ProfilViewState();
}

class _ProfilViewState extends State<ProfilView> {
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    authProvider.setUserProfileInfo();

    return Expanded(
      child: Container(
        margin: EdgeInsets.symmetric(
          horizontal: 10,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //Avatar + nom + role
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                //image avatar
                Container(
                  child: SvgPicture.asset(
                    "assets/images/default_avatar_profile.svg",
                    fit: BoxFit.fill,
                  ),
                ),
                //Nom et rôle
                Column(
                  children: [
                    Text(
                      'Jonathan DIEKE',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 23,
                        fontFamily: "Seravek",
                      ),
                    ),
                    Text(
                      'Ingénieur Développeur',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 15,
                        fontFamily: "MontSerrat",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                )
              ],
            ),
            SizedBox(height: 30),
            // Les informations du profil utilisateur
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //Informations title
                  Text(
                    'Informations',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                      fontFamily: "Seravek",
                    ),
                  ),
                  SizedBox(height: 10),
                  // Détails Informations
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      informationDetail(
                          FontAwesomeIcons.user,
                          "Nom",
                          (authProvider.user.firstName ?? "DIEKE")
                              .toUpperCase()),
                      informationDetail(FontAwesomeIcons.user, "Prénom",
                          (authProvider.user.lastName ?? "Jonathan")),
                      informationDetail(FontAwesomeIcons.phoneAlt, "Téléphone",
                          "  01 53 48 88 36"),
                      informationDetail(FontAwesomeIcons.envelope, "Email",
                          "ange.dieke@gs2e.ci"),
                    ],
                  ),
                  GestureDetector(
                    onTap: () {
                      authProvider.unsetSession().then((_) {
                        print('Déconnexion...');
                        Navigator.pushReplacementNamed(context, "/login");
                      });
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 30),
                      decoration: BoxDecoration(
                        color: Colors.red.shade700,
                        borderRadius: BorderRadius.all(
                          Radius.elliptical(10, 10),
                        ),
                      ),
                      child: Container(
                        constraints: BoxConstraints(
                          minWidth: screenSize.width,
                          minHeight: 50.0,
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          "Déconnexion",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //Bouton déconnexion
          ],
        ),
      ),
    );
  }

  Widget informationDetail(IconData icon, String libel, String detail) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        //Icone + Libellé
        Container(
          margin: EdgeInsets.symmetric(
            vertical: 15,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.max,
            children: [
              Padding(
                padding: EdgeInsets.only(
                  bottom: 5,
                  right: 5,
                  left: 5,
                ),
                child: Icon(
                  icon,
                  size: 30,
                  color: Color(0xff656D74),
                ),
              ),
              Text(
                ' $libel ',
                style: TextStyle(
                    fontSize: 20,
                    color: Color(0xff656D74),
                    fontWeight: FontWeight.bold,
                    fontFamily: "Seravek"),
              )
            ],
          ),
        ),
        Text(
          truncateText(detail, 25),
          style: TextStyle(
            color: Color(0xff656D74),
            fontSize: 20,
            fontFamily: "Montserrat",
            fontWeight: FontWeight.w400,
          ),
        )
      ],
    );
  }
}
