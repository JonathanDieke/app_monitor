import 'package:app_monitor/api/providers/alerte.provider.dart';
import 'package:app_monitor/api/providers/auth.provider.dart';
import 'package:app_monitor/api/providers/transaction.provider.dart';
import 'package:app_monitor/utils/local_notifications_manager.dart';
import 'package:app_monitor/utils/routes.dart';
import 'package:app_monitor/views/splash_screen_2.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:workmanager/workmanager.dart';

// void callbackDispatcher() {
//   Workmanager().executeTask((taskName, inputData) {
//     print(
//         "\t---- Native called background task ----"); //simpleTask will be emitted here.
//     print(DateTime.now());
//     print('taskname : $taskName');
//     var localNotify = LocalNotifyManager.init();
//     localNotify.runNotifications();
//     return Future.value(true);
//   });
// }

void callbackDispatcher(BuildContext context) {
  AlerteProvider alerteProvider =
      Provider.of<AlerteProvider>(context, listen: false);

  alerteProvider.getAlertList().then((value) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    if (sp.getInt("nbAlertesItem") != null) {
      if (alerteProvider.alerteList.length > sp.getInt("nbAlertesItem")) {
        sp.setInt("nbAlertesItem", alerteProvider.alerteList.length);
        Workmanager().executeTask((taskName, inputData) {
          var localNotify = LocalNotifyManager.init();
          localNotify.runNotifications(
              applicationTitle:
                  alerteProvider.alerteList.first.applicationTitle);
          return Future.value(true);
        });
      }
    }
  });
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  

  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  
  

  @override
  Widget build(BuildContext context) { 
    // Workmanager().initialize(
    //   callbackDispatcher, // The top level function, aka callbackDispatcher
    // );
    // Workmanager().registerPeriodicTask(
    //   "666", "alert loop task",
    //   // When no frequency is provided the default 15 minutes is set.
    //   // Minimum frequency is 15 min. Android will automatically change your frequency to 15 min if you have configured a lower frequency.
    //   frequency: Duration(minutes: 15),
    //   // initialDelay: Duration(seconds: 10,)
    // ); //Android only (see below)

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) {
          return AuthProvider();
        }),
        ChangeNotifierProvider(create: (context) {
          AlerteProvider alerteProvider = AlerteProvider();
          return alerteProvider;
        }),
        ChangeNotifierProvider(create: (context) {
          return TransactionProvider();
        }),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        routes: routes,
        title: 'AppMonitor',
        theme: ThemeData(
          primarySwatch: Colors.deepOrange,
        ).copyWith(
            pageTransitionsTheme: PageTransitionsTheme(
                builders: <TargetPlatform, PageTransitionsBuilder>{
              TargetPlatform.android: ZoomPageTransitionsBuilder()
            })),
        home: MySplashScreen(),
      ),
    );
  }
}
