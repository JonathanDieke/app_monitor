import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class LocalNotifyManager {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  var initSettings, androidInitSettings, iosInitSettings;

  LocalNotifyManager.init() {
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    if (Platform.isIOS) {
      requestIOSPermission();
    }
    initializePlatform();
  }

  runNotifications({String applicationTitle}) async {
    var scheduledNotificationDateTime =
        DateTime.now().add(Duration(seconds: 30));

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      "alerte_notif",
      "alerte_notif",
      "Channel pour la notification des alertes",
      icon: "godseye_logo",
      sound: RawResourceAndroidNotificationSound("notif_sound"),
      largeIcon: DrawableResourceAndroidBitmap("godseye_logo"),
    );

    var iosPlatformChannelSpecifics = IOSNotificationDetails(
      sound: "notif_sound.wave",
      presentAlert: true,
      presentBadge: true,
      presentSound: true,
    );

    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iosPlatformChannelSpecifics);

    Timer(Duration(seconds: 35), () async {
      await flutterLocalNotificationsPlugin.show(
        0,
        "GodsEye",
        applicationTitle != null
            ? applicationTitle
            : "APINMPF : Nouvelle alerte...",
        // scheduledNotificationDateTime,
        platformChannelSpecifics,
      );
    });
  }

  requestIOSPermission() {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        .requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
  }

  initializePlatform() async {
    //android settings
    androidInitSettings = AndroidInitializationSettings("godseye_logo");
    //ios settings
    iosInitSettings = IOSInitializationSettings(
        requestAlertPermission: true,
        requestBadgePermission: true,
        requestSoundPermission: true,
        onDidReceiveLocalNotification:
            (int id, String title, String body, String payload) async {});
    //global settings
    initSettings = InitializationSettings(
        android: androidInitSettings, iOS: iosInitSettings);

    //run
    await flutterLocalNotificationsPlugin.initialize(initSettings,
        onSelectNotification: (String payload) async {
      if (payload != null) {
        debugPrint("notification payload : " + payload);
      }
    });
  }
}
