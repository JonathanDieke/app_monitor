class Constants {
  //Constantes here
  static const BASE_URL = "http://vite-bd.com/MonitNmpf/api";

  static const LOG_IN_URL = "$BASE_URL/acces/connexion";
  static const ALERTE_URL = "$BASE_URL/controles/liste";
  static const RAPPORT_URL = "$BASE_URL/nmpf/stats";
  static const LOG_OUT_URL = "$BASE_URL/mon_espace/deconnexion";

  static const double MY_DIALOG_BOX_PADDING = 5;
  static const double  MY_DIALOG_BOX_AVATAR_RADIUS = 45;

  //
}
