
import 'package:app_monitor/views/home/home.dart';
import 'package:app_monitor/views/intro/intro_view.dart';
import 'package:app_monitor/views/login/login_view.dart';
import 'package:flutter/material.dart';

final routes = {
  '/login': (BuildContext context) => LoginView(), 
  '/intro' : (BuildContext context) => IntroView(),
  '/home' : (BuildContext context) => HomeView(),
};
