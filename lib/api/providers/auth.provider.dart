import 'dart:convert';

import 'package:app_monitor/utils/constants.dart';
import 'package:http/http.dart' as http;
import 'package:app_monitor/api/models/user.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthProvider extends ChangeNotifier {
  User user = User();
  SharedPreferences prefs;

  Future<bool> authenticate(
      String email, String password, dynamic isRememberMe) async {
    isRememberMe = isRememberMe ? 1 : 0;
    Map<String, dynamic> data = {
      'login': email,
      'password': password,
      "remember": isRememberMe, 
      "device" : "Samsung A2",
    };
    bool _result = false;

    prefs = await SharedPreferences.getInstance();
    prefs.setBool("userAttemptedConnection", true);

    var responseAPI = await http.post(
      Uri.parse(Constants.LOG_IN_URL),
      headers: {
        "Content-Type": "application/json",
      },
      body: jsonEncode(data),
    );

    if (responseAPI.statusCode == 200) {
      var data = json.decode(responseAPI.body)["data"];

      user = User.fromJson(data);

      setSession(user);

      _result = true;
    }

    return _result;
  }

  Future<String> getUserStatus() async {
    prefs = await SharedPreferences.getInstance();

    if (prefs.getBool("userAttemptedConnection") == null) {
      //première ouverture de l'app
      prefs.setBool("userAttemptedConnection", false);
      return 'intro';
    } else {
      if (prefs.getBool("userAttemptedConnection") == false) {
        //jamais essayé de s'authentifier
        return 'intro';
      } else {
        // déja essayé de s'authentifier
        if (prefs.getString("userToken") != null &&
            prefs.getString("userToken").isNotEmpty) {
          //déjà authentifié
          return 'home';
        }
        // non auth
        return "login";
      }
    }
  }

  String getToken() {
    return prefs.getString("userToken");
  }

  Future<void> setUserProfileInfo() async {
    if (user.email == null || user.firstName == null || user.lastName == null) {
      prefs = await SharedPreferences.getInstance();
    }

    if (user.email == null) {
      user.email = prefs.getString("userEmail");
    }
    if (user.firstName == null) {
      user.firstName = prefs.getString("userFirstname");
    }
    if (user.lastName == null) {
      user.lastName = prefs.getString("userLastname");
    }
  }

  Future<void> unsetSession() async {
    prefs = await SharedPreferences.getInstance();
    String token = "Bearer " + prefs.getString("userToken");

    await http.get(
      Uri.parse(Constants.LOG_OUT_URL),
      headers: {"Authorization": token},
    );
    prefs.clear();
  }

  setSession(User user) async {
    prefs = await SharedPreferences.getInstance();

    prefs.setString('userToken', user.token);
    prefs.setString('userEmail', user.email);
    prefs.setString('userLastname', user.lastName);
    prefs.setString('userFirstname', user.firstName);
  }

  //fin
}
