class User {
  String request,
      id,
      login,
      email,
      firstName,
      lastName,
      address,
      contact,
      enable,
      adActivated,
      mustChangePassword,
      adServer,
      locked,
      totalRetry,
      profile,
      profileId,
      profileTitle,
      profileDescription,
      token;

  User({
    this.request,
    this.id,
    this.login,
    this.email,
    this.firstName,
    this.lastName,
    this.address,
    this.contact,
    this.enable,
    this.adActivated,
    this.mustChangePassword,
    this.adServer,
    this.locked,
    this.totalRetry,
    this.profile,
    this.profileId,
    this.profileTitle,
    this.profileDescription,
    this.token,
  });
  User.fromJson(Map<String, dynamic> json) {
    this.request = json["MEMBER_REQUEST"];
    this.id = json["MEMBER_ID"];
    this.login = json["MEMBER_LOGIN"];
    this.email = json["MEMBER_EMAIL"];
    this.firstName = json["MEMBER_FIRST_NAME"];
    this.lastName = json["MEMBER_LAST_NAME"];
    this.address = json["MEMBER_ADDRESS"];
    this.contact = json["MEMBER_CONTACT"];
    this.enable = json["MEMBER_ENABLE"];
    this.adActivated = json["MEMBER_AD_ACTIVATED"];
    this.mustChangePassword = json["MEMBER_MUST_CHANGE_PASSWORD"];
    this.adServer = json["MEMBER_AD_SERVER"];
    this.locked = json["MEMBER_LOCKED"];
    this.totalRetry = json["MEMBER_TOTAL_RETRY"];
    this.profile = json["MEMBER_PROFILE"];
    this.profileId = json["PROFILE8ID"];
    this.profileTitle = json["PROFILE_TITLE"];
    this.profileDescription = json["PROFILE_DESCRIPTION"];
    this.token = json["token"];
  }

  @override
  String toString() {
    return "${this.login}/${this.token}";
  }
}
