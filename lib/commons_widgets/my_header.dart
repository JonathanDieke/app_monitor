import 'package:app_monitor/commons_widgets/logo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MyHeader extends StatelessWidget {
  MyHeader(this.screenSize, this.titleView, this.icon,
      {this.setTitleView = true, this.setArrowBack = true});

  final Size screenSize;
  final String titleView;
  final String icon;
  final bool setTitleView, setArrowBack;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        //App Bar
        Container(
          height: screenSize.height * 0.13,
          width: screenSize.width,
          padding: EdgeInsets.only(left: 10, right: 10, top: 50),
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              this.setArrowBack
                  ? GestureDetector(
                      onTap: () {
                        print('tap on back icon from dashboard');
                      },
                      child: Icon(
                        Icons.arrow_back_ios_rounded,
                        size: 25,
                      ),
                    )
                  : SizedBox(
                      height: 0,
                      width: 0,
                    ),
              Expanded(
                child: logoMin(),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 10,
        ),
        //titre de la vue
        this.setTitleView
            ? Container(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      this.titleView,
                      style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.w900,
                        fontFamily: "Seravek",
                      ),
                    ),
                    SvgPicture.asset(
                      this.icon,
                      color: Colors.black,
                      height: 40,
                      width: 40,
                    )
                  ],
                ),
              )
            : SizedBox(
                height: 0,
              ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
