import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

logoMin() {
  // return logoFull2();
  return SvgPicture.asset("assets/images/logo_min.svg");
}

logoFull() {
  return SvgPicture.asset("assets/images/logo_full.svg");
}

logoFull2() {
  return SvgPicture.asset("assets/images/logo_full_2.svg");
}

iconLauncher() {
  return Image.asset(
    'assets/images/ic_launcher.png',
    height: 100,
    width: 100,
    fit: BoxFit.fill,
  );
}
