import 'package:flutter/material.dart';

class MyBottomNavigationBar extends StatelessWidget {
  MyBottomNavigationBar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      elevation: 10,
      backgroundColor: Colors.red.shade300,
      items: [
        BottomNavigationBarItem(
          backgroundColor: Colors.red.shade300,
          label: "Home",
          icon: Icon(
            Icons.home_rounded,
            size: 37,
            color: Colors.grey,
          ),
        ),
        BottomNavigationBarItem(
          label: "Rapport",
          icon: Icon(
            Icons.home_rounded,
            size: 37,
            color: Colors.grey,
          ),
        ),
        BottomNavigationBarItem(
          label: "Dashboard",
          icon: Icon(
            Icons.dashboard_rounded,
            size: 37,
            color: Colors.grey,
          ),
        ),
        BottomNavigationBarItem(
          label: "Profil",
          icon: Icon(
            Icons.person_rounded,
            size: 37,
            color: Colors.grey,
          ),
        ),
      ],
    );
    // Container(
    //   padding: EdgeInsets.symmetric(
    //     vertical: 10,
    //     horizontal: 0,
    //   ),
    //   decoration: BoxDecoration(
    //     color: Colors.white70,
    //     borderRadius: BorderRadius.only(
    //       topLeft: Radius.circular(30),
    //       topRight: Radius.circular(30),
    //     ),
    //   ),
    //   child: Row(
    //     mainAxisAlignment: MainAxisAlignment.spaceAround,
    //     children: [
    //       Icon(
    //         Icons.home_rounded,
    //         size: 37,
    //         color: Colors.grey,
    //       ),
    //       Icon(
    //         Icons.home_rounded,
    //         size: 37,
    //         color: Colors.grey,
    //       ),
    //       Icon(
    //         Icons.dashboard_rounded,
    //         size: 37,
    //         color: Colors.deepOrange,
    //       ),
    //       Icon(
    //         Icons.person_rounded,
    //         size: 37,
    //         color: Colors.grey,
    //       ),
    //     ],
    //   ),
    // );
  }
}
