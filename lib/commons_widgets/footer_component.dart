import 'package:flutter/material.dart';

class FooterComponent extends StatelessWidget {
  const FooterComponent({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            '\u00a9 Copyright ${DateTime.now().year} - GS2E - DDI , tous droits réservés',
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
